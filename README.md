Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?
1.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
1.  What does Assertions.assertThrows do?
1.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
    2.  Why do we need to override constructors?
    3.  Why we did not override other Exception methods?	
1.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
1.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
1.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
1.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
1.  Make a printScreen of your eclipse Maven test run, with console
1.  What category of Exceptions is TimerException and what is NullPointerException
1.  Push the updated/fixed source code to your own repository.
  
  
  
* The logger doesn't necessarily print to the console.'
* It came from the ConditionEvaluator.
* It ensures that the specified exception is thrown when a specified piece of code executes.
* TimerException:
 * For many unclear reasons, it is a good idea to include. Its documentation said something about checking compatability between different versions of the class.
 * To ensure that its superclasse's constructor is called. The compiler would create a blank constructor by default.
 * Our custom exception will inherit the other methods from its superclass.
* The static block will be executed when the class is initialized. The contents of this static block initialize our logger with the specified configurations.
* Bitbucket uses the markdown file format. If there is a README.md at the root level of a project, Bitbucket will display it on the repo's source page.'
* The finally block used the variable "timeNow" which could be null if an exception was thrown. Using a null value then throws an exception itself.
* A TimerException is thrown, caught, and then logged in that catch block. The same catch block then throws a new TimerException. When the finally block was there, it would then attempt to use the value "timeNow", but since an exception was thrown in the try block, it never got set. Thus, using it throws a NullPointerException, which isn't caught.
* (See attatched)
* (See attatched)
* TimerException is checked, and NullPointerException is unchecked.
* https://bitbucket.org/Jameson_Axton/lab9b/src/master/